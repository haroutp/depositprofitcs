﻿using System;

namespace DepositProfit
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        static int depositProfit(int deposit, int rate, int threshold) {
            // keep track of years to reach threshold
            int yearsToThreshold = 0;
            
            // converts int to float
            float d = deposit;
            
            
            while(d < threshold){
                d+= ((d * rate)/ 100);
                
                
                yearsToThreshold++;
            }
            
            return yearsToThreshold;
        }

    }
}
